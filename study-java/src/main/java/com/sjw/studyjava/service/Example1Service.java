package com.sjw.studyjava.service;

import com.sjw.studyjava.model.Example1Item;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class Example1Service {
    /**
     * 샘플 데이터를 생성한다.
     *
     * @return ExampleItem 리스트
     */
    private List<Example1Item> settingData() {
        List<Example1Item> result = new LinkedList<>(); // 하나의 Example1Item 리스트다.

        Example1Item item1 =new Example1Item(); // 새로운 아이템1을 보여준다.
        item1.setName("홍길동"); // 아이템1의 이름은 홍길동이다
        item1.setBirthday(LocalDate.of(2022,1,1)); // 아이템1의 홍길동은 2022년 1월 1일에 태어났다.
        result.add(item1); //아이템1을 Example1Item 리스트에 넣는다.

        Example1Item item2 =new Example1Item(); // 새로운 아이템2을 생성한다.
        item2.setName("김철수"); // 아이템2의 이름은 김철수다.
        item2.setBirthday(LocalDate.of(1993,1,1)); // 아이템2의 김철수는 1993년 1월 1일에 태어났다.
        result.add(item2);//아이템2을 Example1Item 리스트에 넣는다.

        Example1Item item3 =new Example1Item(); // 아이템3을 생성한다.
        item3.setName("박이슬"); //아이템3의 이름은 박이슬이다.
        item3.setBirthday(LocalDate.of(1996,1,1)); // 아이템3의 박이슬은 1996년 1월 1일에 태어났다.
        result.add(item3); // 아이템3을 Example1Iem 리스트에 녛는다.

        Example1Item item4 =new Example1Item(); // 아이템4을 생성한다.
        item4.setName("밤하늘"); // 아이템4의 이름은 밤하늘이다.
        item4.setBirthday(LocalDate.of(2021,1,1)); // 아이템4의 밤하늘은 2021년 1월 1일에 태어났다.
        result.add(item4); // 아이템4을 Example1Item 리스트에 넣는다.

        Example1Item item5 =new Example1Item();
        item5.setName("박기태"); // 아이템5를 생성한다.
        item5.setBirthday(LocalDate.of(2020,1,1)); // 아이템5의 박기태는 2020년 1월 1일에 태어났다.
        result.add(item5); //아이템5을 Exampl1Item 리스트에 넣는다.

        return result;

    }
}
